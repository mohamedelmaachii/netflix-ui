import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { HttpClient,HttpHeaders  } from '@angular/common/http';
import { delay, finalize } from 'rxjs/operators';
import {ThemePalette} from '@angular/material/core';
import {ProgressSpinnerMode} from '@angular/material/progress-spinner';
 
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  trendsMovies:Array<String>;
  similarsMovies:Array<String>;
  isLoading:boolean;
  title:string;


  movieIdFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  color: ThemePalette = 'warn';
  mode: ProgressSpinnerMode = 'indeterminate';
  value = 50;
  constructor(private http:HttpClient) {
    this.trendsMovies=[];
    this.similarsMovies=[];
    this.title="";
    
   }

  topTrending()
  {
    console.log('topTrending')
    this.trendsMovies=[];
    this.isLoading=true;
    this.http.get<any>('http://165.22.233.161:5000/trending_movies').pipe(delay(3000)).
    pipe(finalize(()=>this.isLoading=false)).
     subscribe(data=>{this.trendsMovies=data.data;console.log(data)})
  }

  similarMovies()
  {
    console.log('similarMovies')
    console.log(this.movieIdFormControl.value)
    this.similarsMovies=[];
    this.isLoading=true;
    this.http.post<any>('http://165.22.233.161:5000/content_based',{title:this.title}).pipe(delay(3000)).
    pipe(finalize(()=>this.isLoading=false)).
     subscribe(data=>{this.similarsMovies=data.data;console.log(data)})
  }

  ngOnInit(): void {
    this.topTrending();

  }

}
